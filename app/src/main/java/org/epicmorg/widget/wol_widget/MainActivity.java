package org.epicmorg.widget.wol_widget;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity implements View.OnClickListener {
    public static final String KEY_PREFERENCE = "ip_mac";
    public static final String KEY_IP = "key_ip";
    public static final String KEY_MAC = "key_mac";
    public static final String KEY_INDICATOR = "key_indicator";

    private TextView textView;
    private Button button;
    private EditText ipEditText, macEditText, portEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        ipEditText = (EditText) findViewById(R.id.editText);
        macEditText = (EditText) findViewById(R.id.editText2);
        portEditText = (EditText) findViewById(R.id.editText3);

        String[] strings = loadIPAddressMacAddress();
        if (strings != null) {
            ipEditText.setText(strings[0]);
            macEditText.setText(strings[1]);
        }
        if (savedInstanceState != null) {
            textView.setText(savedInstanceState.getString(KEY_INDICATOR));
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(KEY_INDICATOR, textView.getText().toString());
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onResume() {
        super.onResume();
        button.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        button.setOnClickListener(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveIPAddressMacAddress(ipEditText.getText().toString(), macEditText.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear_address:
                ipEditText.getText().clear();
                macEditText.getText().clear();
                textView.setText("Address removed");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        new AsyncTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                return (
                        portEditText.getText().toString() == null
                                || portEditText.getText().length() == 0
                ) ?
                        WakeOnLan.wakeUp(params[0], params[1])
                        : WakeOnLan.wakeUp(params[0], Integer.parseInt(params[2]), params[1]);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                textView.setText(aBoolean ? "Package send" : "Error");
            }
        }.execute(ipEditText.getText().toString(), macEditText.getText().toString(), portEditText.getText().toString());
    }

    private void saveIPAddressMacAddress(String ipAddress, String macAddress) {
        getSharedPreferences(KEY_PREFERENCE, MODE_PRIVATE)
                .edit()
                .putString(KEY_IP, ipAddress)
                .putString(KEY_MAC, macAddress)
                .commit();
    }

    private String[] loadIPAddressMacAddress() {
        SharedPreferences preferences = getSharedPreferences(KEY_PREFERENCE, MODE_PRIVATE);
        String ipAddress = preferences.getString(KEY_IP, null);
        String macAddress = preferences.getString(KEY_MAC, null);
        return (ipAddress == null || macAddress == null) ? null : new String[]{ipAddress, macAddress};
    }
}
