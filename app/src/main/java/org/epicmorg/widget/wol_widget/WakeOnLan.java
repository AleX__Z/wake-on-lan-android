package org.epicmorg.widget.wol_widget;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class WakeOnLan {
    public static final int PORT_2 = 7;
    public static final int PORT_1 = 9;

    public static boolean wakeUp(InetAddress ipAddress, int port, byte[] macAddress) {
        DatagramSocket socket = null;
        Throwable throwable = null;
        try {
            byte[] sendPacket = new byte[6 + 16 * macAddress.length];
            for (int i = 0; i < 6; i++)
                sendPacket[i] = (byte) 0xff;
            for (int i = 6, macLength = macAddress.length, l = sendPacket.length; i < l; i += macLength)
                System.arraycopy(macAddress, 0, sendPacket, i, macAddress.length);
           /* DatagramPacket packet =
                    new DatagramPacket(
                            sendPacket,
                            sendPacket.length,
                            ipAddress,
                            PORT_1
                    );*/
            socket = new DatagramSocket();
            socket.send(new DatagramPacket(
                    sendPacket,
                    sendPacket.length,
                    ipAddress,
                    port
            ));
            /*socket.send

            socket.send(new DatagramPacket(
                    sendPacket,
                    sendPacket.length,
                    ipAddress,
                    PORT_2
            ));*/
        } catch (SocketException e) {
            throwable = e;
            return false;
        } catch (IOException e) {
            throwable = e;
            return false;
        } finally {
            if (throwable == null) {
                if (socket != null) {
                    socket.close();
                }
            } else {
                try {
                    if (socket != null) {
                        socket.close();
                    }
                } catch (Throwable unused) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean wakeUp(InetAddress ipAddress, byte[] macAddress) {
        return wakeUp(ipAddress, PORT_1, macAddress);
    }

    public static boolean wakeUp(String ipAddress, int port, String macAddress) {
        try {
            return wakeUp(InetAddress.getByName(ipAddress), port, getMacBytes(macAddress));
        } catch (Exception e) {
            return false;
        }
    }


    public static boolean wakeUp(String ipAddress, String macAddress) {
        return wakeUp(ipAddress, PORT_1, macAddress);
    }

    private static byte[] getMacBytes(String macAnddress) throws IllegalArgumentException {
        byte[] bytes = new byte[6];
        String[] hex;
        if (macAnddress.length() != 12)
            hex = macAnddress.split("(\\:|\\-)");
        else {
            hex = new String[6];
            for (int i = 0; i < 6; i++)
                hex[i] = macAnddress.substring(2 * i, 2 * i + 1);
        }
        if (hex.length != 6) {
            throw new IllegalArgumentException("Invalid MAC address.");
        }
        try {
            for (int i = 0; i < 6; i++) {
                bytes[i] = (byte) Integer.parseInt(hex[i], 16);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid hex digit in MAC address.");
        }
        return bytes;
    }
}
